import random


class Card():
    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit

    def card_value(self):
        """ Повертає кількість очків, які дає карта"""

        if self.rank in 'TJQK':
            # Кількість очків 10 за десятку, валета, даму і короля
            return 10
        else:
            # Повертає потрібну кількість очків за будьяку іншу карту
            # Туз дає одне очко.
            return ' A23456789'.index(self.rank)

    def get_rank(self):
        return self.rank

    def __str__(self):
        return f'{self.rank}{self.suit}'


class Player():
    def __init__(self, name):
        # назва гравця
        self.name = name
        # початкова кількість карт у гравця
        self.cards = []

    def add_card(self, card):
        """ Дає карту гравцю """
        self.cards.append(card)

    def get_value(self):
        """ Метод отримання кількості очків у гравця """
        result = 0
        # Кількість тузів у гравця.
        aces = 0
        for card in self.cards:
            result += card.card_value()
            # Якщо є туз - збільшуємо кількість тузів
            if card.get_rank() == 'A':
                aces += 1
        # Визначаємо кількість очків за туз 1 або 11
        if result + aces * 10 <= 21:
            result += aces * 10
        return result

    def __str__(self):
        text = f"{self.name} contains:\n"
        for card in self.cards:
            text += str(card) + " "
        text += f"\nКількість набраних очок: {str(self.get_value())}"
        return text


class Game(Card, Player):
    def __init__(self):
        # ранги
        ranks = "23456789TJQKA"
        # масті
        suits = [chr(9829), chr(9830), chr(9824), chr(9827)]

        # генератор списків створює колоду із 52 карт
        self.cards = [Card(r, s) for r in ranks for s in suits]
        # тасуємо колоду
        random.shuffle(self.cards)

    def deal_card(self):
        """ Функція здачі карти """
        return self.cards.pop()

    def new_game(self):
        # створюємо колоду
        d = Game()
        # задаєм карти для гравця і диллера
        name1 = input("Ваше і'мя ")
        player = Player(f"Player {name1}")
        dealer = Player("Dealer")
        # здаєм дві карти гравцю
        player.add_card(d.deal_card())
        player.add_card(d.deal_card())
        # здаєм одну карту диллеру
        dealer.add_card(d.deal_card())
        print(dealer)
        print("="*20)
        print(player)
        # Перевіряємо необхідність продовжувати гру
        in_game = True
        # набирає карти гравець, якшо в нього меньше 21 очка
        while player.get_value() < 21:
            result = input("Hit or stand? (h/s) ")
            if result == "h":
                player.add_card(d.deal_card())
                print(player)

                if player.get_value() > 21:
                    print(f"{name1} програв!!!")
                    in_game = False
            else:
                print(f"{name1} стоїш!")
                break
        print("=" * 20)
        if in_game:
            # Диллер набирає карти поки його рахунок меньший 17 очок
            while dealer.get_value() < 17:
                dealer.add_card(d.deal_card())
                print(dealer)
                if dealer.get_value() > 21:
                    print("Dealer bust")
                    in_game = False
        if in_game:
            # Якщо у диллера і гравця рівна кількість очкок - переможе казино
            if player.get_value() > dealer.get_value():
                print(f"{name1} виграв!")
            else:
                print("Виграв Dealer!")


if __name__ == '__main__':
    g = Game()
    g.new_game()
